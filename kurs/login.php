<?php
    require "db.php";

    $data = $_POST;

    if ( isset($data['do_login']) )
    {
        $errors[] = array();
        unset($errors[0]);
        $user = R::findOne('users', 'login = ?', array($data['login']));
//        var_dump($errors);
        if ( $user )
        {
//            var_dump($errors);
            if ( password_verify($data['password'] , $user->password) )
            {
                $_SESSION['logged_user'] = $user;
                echo '<div style="color: green; font-size: 15px; padding: 8px; font-weight: 600;">Здравствуйте, '.$_SESSION['logged_user']->login.' Вы успешно авторизовались. Перейти на <a href="index.html">Главную страницу</a></div><hr>';
            } else
            {
                $errors[] = 'Неверно введен пароль.';
            }
        } else
        {
            $errors[] = 'Пользователь с таким логином не найден.';
        }
//        var_dump($errors);
        if ( ! empty($errors) )
        {
            echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
        }

    }


?>
<link rel="stylesheet" href="css/style.css" type="text/css"/>
<title>HC GOMEL - Логин</title>

<style>
    .p-input {
        font-size: 14px;
        padding: 15px;
        color: #fff;
    }

    input {
        padding: 5px;
        width: 20%;
        border-radius: 10px;
    }

    button {
        width: 20%;
        border-radius: 10px;
        font-size: 14px;
        height: 40px;
        margin-top: 20px;
    }

    .go-main {
        margin-right: 20px;
        margin-top: 10px;
    }

    .abc {
        margin-top: 15px;
    }
</style>
	<!-- Header -->
	<header id="header">
		<div class="shell">
			<div class="matchPast">
				<h3>Прошедшие матчи</h3>
				<ul class="clearFix">
					<li>
						<div>
							<a><h4>02.04.2020</h4>
							<p class="teamHome"><b>Гомель</b><span class="spans">3</span></p>
							<p class="teamAway">Металлург<span class="spansW">4</span></p>
						</a></div>
					</li>
					<li class="probel">
						<a><h4>01.04.2020</h4>
						<p class="teamHome">Неман<span class="spansW">4</span></p>
						<p class="teamAway"><b>Гомель</b><span class="spans">2</span></p>
						</a>
					</li>
					<li class="probel">
						<a><h4>27.03.2020</h4>
						<p class="teamHome">Лида<span class="spans">0</span></p>
						<p class="teamAway"><b>Гомель</b><span class="spansW">7</span></p>
						</a>
					</li>			
				</ul>
			</div>

			<div class="matchPast">
				<h3>Ближайшие матчи</h3>
				<ul class="clearFix">

					<li>
						<a><h4>?</h4>
							<p class="teamHome">Могилев<span class="spans">?</span></p>
							<p class="teamAway"><b>Гомель</b><span class="spans">?</span></p>
						</a>
					</li>
					<li class="probel">
						<a><h4>?</h4>
							<p class="teamHome">Юность<span class="spans">?</span></p>
							<p class="teamAway"><b>Гомель</b><span class="spans">?</span></p>
						</a>
					</li>
					<li class="probel">
						<a><h4>?</h4>
							<p class="teamHome"><b>Гомель</b><span class="spans">?</span></p>
							<p class="teamAway">Шахтер<span class="spans">?</span></p>
						</a>
					</li>			
				</ul>
			</div>
		</div>
	</header>
	<!-- End Header -->
	
	<!-- Navigation -->
	<div id="navigation">
		<div class="shell">
			<div class="cl"></div>
			<ul>
				<li class="w-auto"><a href="index.html" class="aktiv">Главная</b></a></li>
			    <li class="w-auto"><a href="tablica.html" class="aktiv">Таблица</a></li>
			    <li class="w-auto"><a href="sostav.html" class="aktiv">Состав</a></li>
			    <li class="w-auto"><a href="kalendar.html" class="aktiv">Календарь</a></li>
				<li class="w-auto"><a href="fan.html">Фан-зона</a></li>
				<li class="w-auto"><a href="contacts.html">Контакты</a></li>
				<li class="w-auto"><a class="aktiv" href="login.php">Вход</a></li>
				<li  class="sing-up w-auto"><a class="aktiv" href="singup.php">Регистрация</a></li>
			</ul>
			<div class="cl"></div>
		</div>
	</div>
	<!-- End Navigation -->


<!--Привет, --><?php //echo $_SESSION['logged_user']->login; ?><!-- !-->
<form action="login.php" method="POST" style="text-align: center; background: url(css/images/фон.jpg); background-size: cover; padding: 30px">
	<p class="p-input">Ваш логин</p>
    <input type="text" name="login" value="<?php echo @$data['login']?>">
	<p class="p-input">Ваш пароль</p>
    <input type="password" name="password">
    <p><button title="Регистрация" name="do_login">Войти</button></p>
	<div class="abc"><a class="go-main" style="font-size: 15px" href="/logout.php">Выйти </a> <a style="font-size: 15px" href="index.html"> На главную</a></div>
</form>

