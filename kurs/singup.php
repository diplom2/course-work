<?php
    require_once "db.php"; // подключаю библиотеку RedBeanPhp https://redbeanphp.com/index.php

    $data = $_POST; // всё что передается в php присваивается $data
    if ( isset($data['do_signup']) ) // isset - проверка на непустой массив
    {
        // здесь делается регистрация
        $errors[] = array(); //создаю массив ошибок
        unset($errors[0]);

        if ( trim($data['login']) == '' ) // trim - убирает лишние проблелы
        {
            $errors[] = 'Вы не ввели логин.'; // добавляю ошибку в массив
        }

        if ( trim($data['email']) == '' )
        {
            $errors[] = 'Вы не ввели email.'; // добавляю ошибку в массив
        }

        if ( $data['password'] == '' )
        {
            $errors[] = 'Вы не ввели пароль.'; // добавляю ошибку в массив
        }

        if ( $data['password_2'] == '' )
        {
            $errors[] = 'Вы не ввели пароль повторно.'; // добавляю ошибку в массив
        } else

        if ( $data['password'] !== $data['password_2'] ) // сверяем первый и повторный пароль
        {
            $errors[] = 'Ваши пароли не совпадают.'; // добавляю ошибку в массив
        }

        if (R::count('users', "login = ?", array($data['login'])) > 0)
        {
            $errors[] = 'Пользователь с таким логином уже существует';
        }

        if (R::count('users', "email = ?", array($data['email'])) > 0)
        {
            $errors[] = 'Пользователь с таким email уже существует';
        }

        if (empty($errors))
        {
            $user = R::dispense('users'); // создаем таблицу в БД
            $user->login = $data['login']; // создаем поле
            $user->email = $data['email'];
            $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
            R::store($user); //создание записи в бд
            echo '<div style="color: green; font-size: 15px; padding: 8px; font-weight: 600;">Вы успешно зарегистрировались</div><hr>';

        } else
        {
            echo '<div style="color: red; font-size: 15px; padding: 8px; font-weight: 600;">'.array_shift($errors).'</div><hr>';
        }

    }
?>
<link rel="stylesheet" href="css/style.css" type="text/css"/>
<title>HC GOMEL - Регистрация</title>

<style>
    .p-input {
        font-size: 14px;
        padding: 15px;
        color: #fff;
    }

    input {
        padding: 5px;
        width: 20%;
        border-radius: 10px;
    }

    button {
        width: 20%;
        border-radius: 10px;
        font-size: 14px;
        height: 40px;
        margin-top: 10px;
    }
</style>
	<!-- Header -->
	<header id="header">
		<div class="shell">
			<div class="matchPast">
				<h3>Прошедшие матчи</h3>
				<ul class="clearFix">
					<li>
						<div>
							<a><h4>02.04.2020</h4>
							<p class="teamHome"><b>Гомель</b><span class="spans">3</span></p>
							<p class="teamAway">Металлург<span class="spansW">4</span></p>
						</a></div>
					</li>
					<li class="probel">
						<a><h4>01.04.2020</h4>
						<p class="teamHome">Неман<span class="spansW">4</span></p>
						<p class="teamAway"><b>Гомель</b><span class="spans">2</span></p>
						</a>
					</li>
					<li class="probel">
						<a><h4>27.03.2020</h4>
						<p class="teamHome">Лида<span class="spans">0</span></p>
						<p class="teamAway"><b>Гомель</b><span class="spansW">7</span></p>
						</a>
					</li>			
				</ul>
			</div>

			<div class="matchPast">
				<h3>Ближайшие матчи</h3>
				<ul class="clearFix">

					<li>
						<a><h4>?</h4>
							<p class="teamHome">Могилев<span class="spans">?</span></p>
							<p class="teamAway"><b>Гомель</b><span class="spans">?</span></p>
						</a>
					</li>
					<li class="probel">
						<a><h4>?</h4>
							<p class="teamHome">Юность<span class="spans">?</span></p>
							<p class="teamAway"><b>Гомель</b><span class="spans">?</span></p>
						</a>
					</li>
					<li class="probel">
						<a><h4>?</h4>
							<p class="teamHome"><b>Гомель</b><span class="spans">?</span></p>
							<p class="teamAway">Шахтер<span class="spans">?</span></p>
						</a>
					</li>			
				</ul>
			</div>
		</div>
	</header>
	<!-- End Header -->
	
	<!-- Navigation -->
	<div id="navigation">
		<div class="shell">
			<div class="cl"></div>
			<ul>
				<li class="w-auto"><a href="index.html" class="aktiv">Главная</b></a></li>
			    <li class="w-auto"><a href="tablica.html" class="aktiv">Таблица</a></li>
			    <li class="w-auto"><a href="sostav.html" class="aktiv">Состав</a></li>
			    <li class="w-auto"><a href="kalendar.html" class="aktiv">Календарь</a></li>
				<li class="w-auto"><a href="fan.html">Фан-зона</a></li>
				<li class="w-auto"><a href="contacts.html">Контакты</a></li>
				<li class="w-auto"><a class="aktiv" href="login.php">Вход</a></li>
				<li  class="sing-up w-auto"><a class="aktiv" href="singup.php">Регистрация</a></li>
			</ul>
			<div class="cl"></div>
		</div>
	</div>
	<!-- End Navigation -->
	
	<!-- Heading -->
<form action="singup.php" method="POST" style="text-align: center; background: url(css/images/фон.jpg); background-size: cover; padding: 30px">
            <p class="p-input">Введите ваш логин</p>
            <input  type="text" name="login" value="<?php echo $data['login']?>">

            <p class="p-input">Введите ваш email</p>
            <input type="email" name="email" value="<?php echo $data['email']?>">

            <p class="p-input">Введите ваш пароль</p>
            <input type="password" name="password" value="<?php echo $data['password']?>">

            <p class="p-input">Повторите введенный пароль</p>
            <input type="password" name="password_2" value="<?php echo $data['password_2']?>">
            <p class="p-input">
                <button title="Регистрация" name="do_signup">Зарегистрироваться</button>
            </p>
            <a href="index.html">На главную</a>
</form>


